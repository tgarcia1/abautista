/*
 * Creado el 18-sep-06
 *
 * Para cambiar la plantilla para este archivo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 */
package com.mapfre.uemp.tssinitron.control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.DynaBean;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mapfre.general.exceptions.MapfreException;

/**
 * @author HERRERD
 *
 * Para cambiar la plantilla para este comentario de tipo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 */
public class ControlFechasAction extends TssiniestrosAction {
	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws MapfreException {

		String ln, pageBean;
		ln = "UEMP.TSSINITRON.CONTROL_FECHAS";
		pageBean = "DatosSalidaControlFechasPageBean";
		ejecutaLN(ln);
		request.setAttribute(pageBean, createPSO(pageBean));
		EvaluaRetorno evalua =
			new EvaluaRetorno(
				(DynaBean) request.getAttribute(pageBean),
				servlet);
		if (evalua.getRetorno().equals("error"))
			request.setAttribute("error", evalua.getError());
		return mapping.findForward(evalua.getRetorno());
	}
}
