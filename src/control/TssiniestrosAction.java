package com.mapfre.uemp.tssinitron.control;

import javax.servlet.http.HttpSession;

import com.mapfre.commons.log.log4j.Log;
import com.mapfre.commons.services.log.Logger;
import com.mapfre.tron21.control.Tron21Action;

/**
 * @author herrerd
 * @version 1 , 23 Nov 2006
 *
 * Clase de control de la que heredar�n todas las clases
 * del proyecto
 * 
 */
public class TssiniestrosAction extends Tron21Action {
	/*
	 * @author herrerd
	 * @params HttpSession sesion
	 * 
	 * M�todo que controla la validez de la sesi�n
	 * */
	private static Logger logger = new Log(TssiniestrosAction.class);
	protected boolean checkSession(HttpSession sesion) {

		if (sesion == null || sesion.isNew()) {
			if (logger.isInfoEnabled()) {
				logger.info("SESI�N CADUCADA");
			}
			return false;
		} else {

			return true;
		}
	}
}
