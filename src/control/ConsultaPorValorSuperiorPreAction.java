package com.mapfre.uemp.tssinitron.control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.DynaBean;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mapfre.general.exceptions.MapfreException;

/**
 * @author HERRERD
 * @version 1 22 Sept 2006
 * 
 *Ejecuta el PL de consulta de siniestros comunicados con valor superior a un valor dado
 */
public class ConsultaPorValorSuperiorPreAction extends TssiniestrosAction {
	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws MapfreException {
		/*
		 * Antes de hacer nada, comprobamos la validez
		 * de la sesi�n*/
		boolean sesionValida = checkSession(request.getSession());
		if (!sesionValida) {
			return mapping.findForward("sesioncaducada");
		} else {

			String ln, pageBean, forward;
			ln = "UEMP.TSSINITRON.COMUNICADOS_VALOR_CARGA";

			forward = "success";
			pageBean = "DatosSComunicadosValorCargaPBean";
			/**
			 * ejecuta la LN y mete el resultado en el request*/
			ejecutaLN(ln);
			request.setAttribute(pageBean, createPSO(pageBean));

			EvaluaRetorno evalua =
				new EvaluaRetorno(
					(DynaBean) request.getAttribute(pageBean),
					servlet);
			if (evalua.getRetorno().equals("error")) {
				request.setAttribute("error", evalua.getError());
				forward = evalua.getRetorno();
			}

			return mapping.findForward(forward);
		}
	}
}