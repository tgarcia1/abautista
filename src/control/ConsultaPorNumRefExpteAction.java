/*
 * Creado el 18-sep-06
 *
 * Para cambiar la plantilla para este archivo generado vaya a
 * Ventana&gt;Preferencias&gt;Java&gt;Generaci�n de c�digo&gt;C�digo y comentarios
 */
package com.mapfre.uemp.tssinitron.control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.DynaBean;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mapfre.commons.log.log4j.Log;
import com.mapfre.commons.services.log.Logger;
import com.mapfre.general.exceptions.MapfreException;
/**
 * 
 * @author herrerd
 *
 * Ejecuta el PL de consulta de siniestros por n�mero de referencia del expediente
 *
 */
public class ConsultaPorNumRefExpteAction extends TssiniestrosAction {
	private static Logger logger = new Log(ConsultaPorNumRefExpteAction.class);
	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws MapfreException {
		/*
		 * Antes de hacer nada, comprobamos la validez
		 * de la sesi�n*/
		boolean sesionValida = checkSession(request.getSession());
		if (!sesionValida) {
			return mapping.findForward("sesioncaducada");
		} else {

			String ln, pageBean;
			ln = "UEMP.TSSINITRON.CONSULTA_EXPEDIENTE";
			pageBean = "DatosSalidaExpCoberPageBean";
			ejecutaLN(ln);
			// Ejecuta la l�gica de negocio y m ete el resultado en el request
			request.setAttribute(pageBean, createPSO(pageBean));

			EvaluaRetorno evalua =
				new EvaluaRetorno(
					(DynaBean) request.getAttribute(pageBean),
					servlet);
			if (evalua.getRetorno().equals("error")) {
				request.setAttribute("error", evalua.getError());
				logger.error("ERROR EJECUTANDO LN [ " + ln + "]");
			}
			return mapping.findForward(evalua.getRetorno());
		}
	}
}