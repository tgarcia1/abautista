package com.mapfre.uemp.tssinitron.control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mapfre.general.exceptions.MapfreException;
import com.mapfre.general.struts.MapfreAction;

/**
* RELLENAR CON UNA EXPLICACIÓN DE LA UTILIDAD DEL ACTION
*
* @author $Author: perezcf $: 
* @version $Revision: 1.4 $: - $Date: 2011/01/13 09:55:05 $: 
*/
public class CerrarAplicacionAction extends MapfreAction {
	/**
	* RELLENAR CON UNA EXPLICACIÓN DE LA UTILIDAD
	*
	* @param mapping instancia de ActionMapping definida en el struts-config para la presente peticion
	* @param form instancia de ActionForm definida en el forumlario para la presente peticion
	* @param request peticion HTTP que ha desencadenado la ejecucion de esta accion
	* @param reponse repuesta HTTP que sera devuelta por esta accion a la peticion
	* @return ActionForward pagina donde sera reeenviada la peticion HTTP 
	*/
	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws MapfreException {

		request.getSession().invalidate();

		return mapping.findForward("success");
	}
}