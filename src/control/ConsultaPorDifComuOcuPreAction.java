package com.mapfre.uemp.tssinitron.control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.DynaBean;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mapfre.commons.log.log4j.Log;
import com.mapfre.commons.services.log.Logger;
import com.mapfre.general.exceptions.MapfreException;

/**
 * 
 * @author herrerd
 * @version 1  - 18 Sept 2006
 *
 *  Clase ConsultaPorDifComuOcuPreAction tan s�lo carga unos datos obtenidos del servidor
 * como son una lista de entidades , mes y a�o deseados y  meses de diferencia
 */
public class ConsultaPorDifComuOcuPreAction extends TssiniestrosAction {
	private static Logger logger =
		new Log(ConsultaPorDifComuOcuPreAction.class);
	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws MapfreException {
		/*
		 * Antes de hacer nada, comprobamos la validez
		 * de la sesi�n*/
		boolean sesionValida = checkSession(request.getSession());
		if (!sesionValida) {
			return mapping.findForward("sesioncaducada");
		} else {

			String ln, pageBean, forward;
			ln = "UEMP.TSSINITRON.DIFERENCIAS_FEC_COMU_CARGA";
			forward = "success";
			pageBean = "CPCDiferenciaComunicaYOcurrenciaPBean";
			ejecutaLN(ln);
			//ejecuta la l�gica de negocio y mete el resultado en el request
			request.setAttribute(pageBean, createPSO(pageBean));

			EvaluaRetorno evalua =
				new EvaluaRetorno(
					(DynaBean) request.getAttribute(pageBean),
					servlet);
			if (evalua.getRetorno().equals("error")) {
				request.setAttribute("error", evalua.getError());
				forward = evalua.getRetorno();
				logger.error("ERROR EJECUTANDO LN [ " + ln + "]");
			}
			return mapping.findForward(forward);
		}
	}
}