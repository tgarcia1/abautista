package com.mapfre.uemp.tssinitron.control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mapfre.general.exceptions.MapfreException;

/**
 * @author herrerd
 * @version 1 , 23 Nov 2006
 *
 * Clase de control de la que heredar�n todas las clases
 * del proyecto 
 * 
 */
public class ForwardSiniestrosAction extends TssiniestrosAction {
	/*
	 * @author herrerd
	 * @params HttpSession sesion
	 * 
	 * M�todo que controla la validez de la sesi�n
	 * */

	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws MapfreException {
		boolean sesionValida = checkSession(request.getSession());
		if (!sesionValida) {
			return mapping.findForward("sesioncaducada");
		} else {
			return mapping.findForward("success");
		}

	}
}
