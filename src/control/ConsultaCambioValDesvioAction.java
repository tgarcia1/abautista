package com.mapfre.uemp.tssinitron.control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.DynaBean;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mapfre.commons.log.log4j.Log;
import com.mapfre.commons.services.log.Logger;
import com.mapfre.general.exceptions.MapfreException;

/**
 * 
 * @author herrerd
 * @version 1 23 Noviembre 2006
 *
 * Ejecuta el PL Cambio_VAL_DESVIO metiendo el resultado (un pageBean) en el request
 */
public class ConsultaCambioValDesvioAction extends TssiniestrosAction {
	private static Logger logger = new Log(ConsultaCambioValDesvioAction.class);

	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws MapfreException {
		boolean sesionValida = checkSession(request.getSession());
		if (!sesionValida) {
			return mapping.findForward("sesioncaducada");
		} else {

			String ln, pageBean;
			ln = "UEMP.TSSINITRON.CAMBIO_VAL_DESVIO";
			pageBean = "DatosSalidaValDevioPageBean";
			ejecutaLN(ln);
			request.setAttribute(pageBean, createPSO(pageBean));

			EvaluaRetorno evalua =
				new EvaluaRetorno(
					(DynaBean) request.getAttribute(pageBean),
					servlet);

			if (evalua.getRetorno().equals("error")) {
				request.setAttribute("error", evalua.getError());
				logger.error("ERROR EN LA CONSULTA DEL LN [" + ln + "]");
			}
			return mapping.findForward(evalua.getRetorno());
		}
	}
}