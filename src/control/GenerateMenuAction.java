package com.mapfre.uemp.tssinitron.control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mapfre.general.exceptions.MapfreException;
import com.mapfre.tron21.control.Tron21Action;

/**
* Redirige la peticion al servlet de menu con la presentacion adecuada.
* La clase requestProcessor se encargar� de modificar la URI del forward en caso de
* que la petici�n provenga de un dispositivo Pocket PC, a�adiendole al final '_PIE'.
* As� conseguimos llamar al servlet de menu de distintas formas: PC (menu.xsl), PDA (menu_PIE.xsl) 
* 
* @author Tecnolog�a - Mapfre Mutualidad
* @version 1.1 17-Enero-2004 
* 
*/
public class GenerateMenuAction extends Tron21Action {

	/**
	* En funci�n de donde proceda la petici�n redirigimos al servlet del menu de una forma u otra.
	* 	- Caso PC:  Se llama al servlet menu, y se transforma el resultado con el "menu.xsl"
	*   - Caso PDA: Se llama al servlet menu, y se transforma el resultado con el "menu_PIE.xsl"
	*     (el "_PIE" se lo a�ade el requestProcessor)
	*  
	* @param mapping Instancia de ActionMapping definida en el struts-config para la presente peticion
	* @param form Instancia de ActionForm definida en el forumlario para la presente peticion
	* @param request Peticion HTTP que ha desencadenado la ejecucion de esta accion
	* @param reponse Respuesta HTTP que sera devuelta por esta accion a la peticion
	* @return ActionForward Pagina donde sera reeenviada la peticion HTTP
	* @author Tecnolog�a - Mapfre Mutualidad
	*/

	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws MapfreException {
		/*
		 * Antes de hacer nada, comprobamos la validez
		 * de la sesi�n*/

		return mapping.findForward("unica");

	}
}