package com.mapfre.uemp.tssinitron.control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.DynaBean;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mapfre.commons.log.log4j.Log;
import com.mapfre.commons.services.log.Logger;
import com.mapfre.general.exceptions.MapfreException;

/**
 * @author HERRERD
 * @version 1 18 Sept 2006
 *Clase que ejecuta el PL MOVIMIENTOS_COBERTURA para obtener el listado de movimientos de una cobertura
 */
public class ConsultaMovimientosCoberturaAction extends TssiniestrosAction {
	private static Logger logger =
		new Log(ConsultaMovimientosCoberturaAction.class);
	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws MapfreException {
		/*
		 * Antes de hacer nada, comprobamos la validez
		 * de la sesi�n*/
		boolean sesionValida = checkSession(request.getSession());
		if (!sesionValida) {
			return mapping.findForward("sesioncaducada");
		} else {

			String ln, pageBean;
			ln = "UEMP.TSSINITRON.MOVIMIENTOS_COBERTURA";
			pageBean = "DatosSalidaMovimCoberPageBean";
			ejecutaLN(ln);
			request.setAttribute(pageBean, createPSO(pageBean));

			EvaluaRetorno evalua =
				new EvaluaRetorno(
					(DynaBean) request.getAttribute(pageBean),
					servlet);
			if (evalua.getRetorno().equals("error")) {
				request.setAttribute("error", evalua.getError());
				logger.error("ERROR EJECUTANDO LN [ " + ln + "]");
			}

			return mapping.findForward(evalua.getRetorno());
		}
	}
}
