package com.mapfre.uemp.tssinitron.control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.DynaBean;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mapfre.commons.log.log4j.Log;
import com.mapfre.commons.services.log.Logger;
import com.mapfre.general.exceptions.MapfreException;

/**
 * 
 * @author herrerd
 * @version 1 
 * 
 * Clase que ejecuta el PL CAMBIO_VAL_DESVIO_CARGA y muestra un listado con los cambios de valoraci�n
 * con una desviaci�n superior a la dada
 * 
 */
public class ConsultaCambioValDesvioPreAction extends TssiniestrosAction {
	private static Logger logger =
		new Log(ConsultaCambioValDesvioPreAction.class);
	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws MapfreException {
		/*
		 * Antes de hacer nada, comprobamos la validez
		 * de la sesi�n*/
		boolean sesionValida = checkSession(request.getSession());
		if (!sesionValida) {
			return mapping.findForward("sesioncaducada");
		} else {

			String ln, pageBean, forward;
			ln = "UEMP.TSSINITRON.CAMBIO_VAL_DESVIO_CARGA";
			forward = "success";
			pageBean = "DatosSalidaValDevioCargaPageBean";
			/**
			 * Ejecuta la l�gica de negocio*/
			ejecutaLN(ln);
			/**
			 * mete el resultado en el request*/
			request.setAttribute(pageBean, createPSO(pageBean));

			EvaluaRetorno evalua =
				new EvaluaRetorno(
					(DynaBean) request.getAttribute(pageBean),
					servlet);

			if (evalua.getRetorno().equals("error")) {
				request.setAttribute("error", evalua.getError());
				forward = evalua.getRetorno();
				logger.error("ERROR EN LA CONSULTA DEL LN [" + ln + "]");
			}
			return mapping.findForward(forward);

		}
	}
}