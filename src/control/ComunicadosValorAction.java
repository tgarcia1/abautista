package com.mapfre.uemp.tssinitron.control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.DynaBean;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.mapfre.commons.log.log4j.Log;
import com.mapfre.commons.services.log.Logger;
import com.mapfre.general.exceptions.MapfreException;

/**
 * Clase que ejecuta el c�digo pl COMUNICADOS_VALOR
 * @author herrerd
 * @version 1 18 Sept 2006
 * 
 * */
public class ComunicadosValorAction extends TssiniestrosAction {
	private static Logger logger = new Log(ComunicadosValorAction.class);
	/**
	 * Ejecuci�n del Action
	 */
	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws MapfreException {
		/**
		 * comprobamos la validez de la sesi�n
		 * */
		boolean sesionValida = checkSession(request.getSession());
		if (!sesionValida) {
			return mapping.findForward("sesioncaducada");
		} else {

			String ln, pageBean;
			ln = "UEMP.TSSINITRON.COMUNICADOS_VALOR";
			pageBean = "DatosSalidaComunicadosValorPageBean";
			/*
			 * Ejecutamos la l�gica de negocio*/
			ejecutaLN(ln);
			request.setAttribute(pageBean, createPSO(pageBean));

			EvaluaRetorno evalua =
				new EvaluaRetorno(
					(DynaBean) request.getAttribute(pageBean),
					servlet);

			if (evalua.getRetorno().equals("error")) {

				request.setAttribute("error", evalua.getError());
				logger.error(
					"ERROR AL EJECUTAR LA LN "
						+ ln
						+ "  \n "
						+ evalua.getError());
			}

			return mapping.findForward(evalua.getRetorno());
				}
	}
}