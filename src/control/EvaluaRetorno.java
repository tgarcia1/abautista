package com.mapfre.uemp.tssinitron.control;

import javax.servlet.http.HttpServlet;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;

/**
 * @author HERRERD
 * @version 1 12 Sept 2006
 *
 * Clase que recibe el c�digo de error que devuelve el PL y lo compara con los c�digos que hay en el archivo
 * codigosRetorno.propeties
 */
public class EvaluaRetorno {
	protected DynaBean db;
	public String retorno = "";
	public String error = "";
	/**
	 * @version 1 23 Sept 2006
	 * @param db DynaBean
	 * @param servlet HttpServlet
	 * 
	 * M�todo que eval�a el retorno devuelto por el PL y lo compara con los c�digos contenidos
	 * en el fichero codigosRetorno.properties.
	 */
	public EvaluaRetorno(DynaBean db, HttpServlet servlet) {
		//recibimos el DynaBean con el PageBean
		DynaClass dc = db.getDynaClass();
		DynaProperty[] dp = dc.getDynaProperties();
		String nameProperty = dp[0].getName();

		DynaBean db2 = (DynaBean) db.get(nameProperty);
		String sCodRetorno = (String) db2.get("COD_RETORNO");
		String sDescRetorno = (String) db2.get("DESC_RETORNO");

		/*
		 * 	Si desde pl no devuelve ning�n cod_retorno, es que algo ha ido mal
		 * */
		if (sCodRetorno == null) {
			sCodRetorno = "999";
			sDescRetorno = "ERROR NO CONTEMPLADO";
		}

		if (sCodRetorno.equals("000") || sCodRetorno.equals("900")) {
			retorno = "success";
		} else {
			retorno = "error";
			setError(sCodRetorno + '|' + sDescRetorno);
		}
	}
	/**
	 * @return retorno: variable booleana con true o false
	 */
	public String getRetorno() {
		return retorno;
	}
	/**
	 * @param string
	 */
	public void setRetorno(String string) {
		retorno = string;
	}

	/**
	 * @return
	 */
	public String getError() {
		return error;
	}

	/**
	 * @param string
	 */
	public void setError(String string) {
		error = string;
	}
}
